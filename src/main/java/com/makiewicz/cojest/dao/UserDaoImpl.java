package com.makiewicz.cojest.dao;

import com.makiewicz.cojest.model.Event;
import com.makiewicz.cojest.model.User;
import com.makiewicz.cojest.model.UserData;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository(value = "userDao")
public class UserDaoImpl extends AbstractDAO implements UserDao {

    @Override
    public void addUser(User user, UserData userData) {
        user.setUserData(userData);
        persist(user);
    }

    @Override
    public Optional<User> getUserById(Long id) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("id",id));

        return Optional.ofNullable((User)criteria.uniqueResult());
    }

    @Override
    public boolean updateUser(User user) {
        Optional<User> optionalUser = getUserById(user.getId());
        if(optionalUser.isPresent()){
            User user1 = optionalUser.get();
            user1.setLogin(user.getLogin());
            user1.setPasswordHash(user.getPasswordHash());
            persist(user1);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean deleteUserById(Long id) {
        Optional<User> optionalUser = getUserById(id);
        if(optionalUser.isPresent()){
            delete(optionalUser.get());
            return true;
        }else{
            return false;
        }
    }

    @Override
    public List<User> getAllUsers() {
        Criteria criteria = getSession().createCriteria(User.class);
        return (List<User>)criteria.list();
    }

    @Override
    public void addEvent(User user, Event event) {
        user.addEvent(event);
        persist(user);
    }
}
