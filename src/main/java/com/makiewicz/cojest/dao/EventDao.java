package com.makiewicz.cojest.dao;

import com.makiewicz.cojest.model.Event;
import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Optional;

public interface EventDao {
    void addEvent(@NotNull Event event);
    Optional<Event> getEventById(@NotNull Long id);
    boolean updateEvent(@NotNull Event event);
    boolean deleteEventById(@NotNull Long id);
    List<Event> getAllEvents();
}
