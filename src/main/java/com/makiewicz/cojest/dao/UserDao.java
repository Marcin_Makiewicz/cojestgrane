package com.makiewicz.cojest.dao;

import com.makiewicz.cojest.model.Event;
import com.makiewicz.cojest.model.User;
import com.makiewicz.cojest.model.UserData;
import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    void addUser(@NotNull User user, UserData userData);
    Optional<User> getUserById(@NotNull Long id);
    boolean updateUser(@NotNull User user);
    boolean deleteUserById(@NotNull Long id);
    List<User> getAllUsers();
    void addEvent(@NotNull User user, @NotNull Event event);
}
