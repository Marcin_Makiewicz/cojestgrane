package com.makiewicz.cojest.dao;

import com.makiewicz.cojest.model.Event;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository(value = "eventDao")
public class EventDaoImpl extends AbstractDAO implements EventDao {

    @Override
    public void addEvent(Event event) {
        persist(event);
    }

    @Override
    public Optional<Event> getEventById(Long id) {
        Criteria criteria = getSession().createCriteria(Event.class);
        criteria.add(Restrictions.eq("id",id));

        return Optional.ofNullable((Event)criteria.uniqueResult());
    }

    @Override
    public boolean updateEvent(Event event) {
        Optional<Event> optionalEvent = getEventById(event.getId());
        if(optionalEvent.isPresent()){
            Event event1 = optionalEvent.get();
            event1.setName(event.getName());
            event1.setDescription(event.getDescription());
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean deleteEventById(Long id) {
        Optional<Event> optionalEvent = getEventById(id);
        if(optionalEvent.isPresent()){
            delete(optionalEvent.get());
            return true;
        }else{
            return false;
        }
    }

    @Override
    public List<Event> getAllEvents() {
        Criteria criteria = getSession().createCriteria(Event.class);
        return (List<Event>)criteria.list();
    }
}
