package com.makiewicz.cojest.model.responses;

public class Result {
    protected Object resultObject;
    protected String message;

    public Result() {
        this.resultObject = null;
        this.message = "OK";
    }

    public Result(Object resultObject) {
        this.resultObject = resultObject;
    }

    public Result(Object resultObject, String message) {
        this.resultObject = resultObject;
        this.message = message;
    }

    public Result(String message) {
        this.resultObject = null;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResultObject() {
        return resultObject;
    }

    public void setResultObject(Object resultObject) {
        this.resultObject = resultObject;
    }
}
