package com.makiewicz.cojest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String login;
    @Column
    private String passwordHash;


    @OneToOne(cascade = CascadeType.PERSIST)
    private UserData userData;

    @OneToMany(mappedBy = "postedByUser" /*, fetch = FetchType.EAGER*/)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Event> eventList;


    public User() {
        this.eventList = new ArrayList<>();
    }

    public User(String login, String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.eventList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                '}';
    }

    public void addEvent (Event event){
        eventList.add(event);
    }
}
