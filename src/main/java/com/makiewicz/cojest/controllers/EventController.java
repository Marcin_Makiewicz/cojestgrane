package com.makiewicz.cojest.controllers;

import com.makiewicz.cojest.model.Event;
import com.makiewicz.cojest.model.responses.Response;
import com.makiewicz.cojest.model.responses.ResponseFactory;
import com.makiewicz.cojest.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/rest")
public class EventController {

    @Autowired
    EventService eventService;

    //example od URL: http://localhost:8080/rest/event/add?eventName=pokaz&description=strzelaja!
    @RequestMapping(value = "/event/add", method = RequestMethod.GET)
    public ResponseEntity<Response> addEvent(@RequestParam String eventName,
                                                 @RequestParam String description){
        Event event = new Event();
        event.setName(eventName);
        event.setDescription(description);
        eventService.addEvent(event);
        return new ResponseEntity<Response>(
                ResponseFactory.success(),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/event/{eventId}", method = RequestMethod.GET)
    public ResponseEntity<Response> showEventWithId(@PathVariable("eventId") Long eventId){
        Optional<Event> eventOptional= eventService.getEventById(eventId);
        if(eventOptional.isPresent()){
            return new ResponseEntity<Response>(
                    ResponseFactory.success(eventOptional.get()),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Event doesn't exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    //example of URL: http://localhost:8080/event/update/1?eventName=przedstawienie&description=cos tam sie dzieje
    @RequestMapping(value = "event/update/{eventId}", method = RequestMethod.GET)
    public ResponseEntity<Response> updateEvent(@PathVariable("eventId") Long eventId,
                                               @RequestParam String eventName,
                                               @RequestParam String description){
        Event event = new Event();
        event.setName(eventName);
        event.setDescription(description);
        event.setId(eventId);
        if(eventService.updateEvent(event)){
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Event doesn't exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/event/delete/{eventId}", method = RequestMethod.GET)
    public ResponseEntity<Response> deleteEventWithId(@PathVariable("eventId") Long eventId) {
        if (eventService.deleteEvent(eventId)) {
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Event doesn't exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/eventsList", method = RequestMethod.GET)
    public ResponseEntity<Response> getEventsList(){
        return new ResponseEntity<Response>(
                ResponseFactory.success(eventService.getAllEvents()),
                HttpStatus.OK);
    }
}
