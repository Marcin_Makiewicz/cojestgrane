package com.makiewicz.cojest.controllers;

import com.makiewicz.cojest.model.Event;
import com.makiewicz.cojest.model.User;
import com.makiewicz.cojest.model.UserData;
import com.makiewicz.cojest.model.responses.Response;
import com.makiewicz.cojest.model.responses.ResponseFactory;
import com.makiewicz.cojest.services.EventService;
import com.makiewicz.cojest.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "/rest")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    EventService eventService;


    @RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    public ResponseEntity<Response> registerUser(@RequestBody User user){
        Logger.getLogger(getClass()).debug("Requested registerUser: " + user);

        if(!userService.userExist(user.getLogin())){
            userService.registerUser(user,user.getUserData());

            return new ResponseEntity<Response>(
                    ResponseFactory.success(user.getId()),
                    HttpStatus.OK);
        }else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with this userName already exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Response> showUserWithId(@PathVariable("userId") Long userId){
        Optional<User> userOptional= userService.getUserById(userId);
        if(userOptional.isPresent()){
            return new ResponseEntity<Response>(
                    ResponseFactory.success(userOptional.get()),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User doesn't exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }


    //example of URL: http://localhost:8080/rest/user/update/1?userName=maks&password=123
    @RequestMapping(value = "user/update/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Response> updateUser(@PathVariable("userId") Long userId,
                                               @RequestParam String userName,
                                               @RequestParam String password){
        User user = new User(userName,password);
        user.setId(userId);
        if(userService.updateUser(user)){
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User doesn't exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/user/delete/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Response> deleteUserWithId(@PathVariable("userId") Long userId) {
        if (userService.deleteUser(userId)) {
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User doesn't exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/usersList", method = RequestMethod.GET)
    public ResponseEntity<Response> requestUserList(){
        return new ResponseEntity<Response>(
                ResponseFactory.success(userService.getAllUsers()),
                HttpStatus.OK);
    }

    //example of URL: http://localhost:8080/rest/user/1/addEvent/?eventName=pokaz&description=strzelaja!
    @RequestMapping(value = "/user/{userId}/addEvent", method = RequestMethod.GET)
    public ResponseEntity<Response> addEvent(
            @PathVariable("userId") Long userId,
            @RequestParam String eventName,
            @RequestParam String description){
        Optional<User> optionalUser = userService.getUserById(userId);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            Event event = new Event();
            event.setName(eventName);
            event.setDescription(description);
            event.setPostedByUser(user);
            eventService.addEvent(event);
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        } else
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exists"),
                    HttpStatus.BAD_REQUEST);
    }

}
