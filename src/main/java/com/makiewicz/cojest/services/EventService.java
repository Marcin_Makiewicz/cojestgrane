package com.makiewicz.cojest.services;

import com.makiewicz.cojest.model.Event;
import com.makiewicz.cojest.model.Location;

import java.util.List;
import java.util.Optional;

public interface EventService {

    //User story: chcę dodać wydarzenie
    boolean addEvent(Event event);

    //User story: chcę znaleźć wydarzenie po określonym id
    Optional<Event> getEventById(Long eventId);

    //User story: chcę wyedytować wydarzenie
    boolean updateEvent(Event event);

    //User story: chcę usunąć wydarzenie o wskazanym id
    boolean deleteEvent(Long eventId);

    //User story: chcę pobrać wszystkie wydarzenia
    List<Event> getAllEvents();

    //User story: chcę pobrać wydarzenia z mojej okolicy
    List<Event> getEventsNearMyLocation(Location location);

    //User story: Chcę uzyskać listę podobnych wydarzeń do wybranego
    List<Event> getSimilarEvents(Event event);
}
