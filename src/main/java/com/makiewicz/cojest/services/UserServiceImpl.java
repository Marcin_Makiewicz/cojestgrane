package com.makiewicz.cojest.services;

import com.makiewicz.cojest.dao.UserDao;
import com.makiewicz.cojest.model.User;
import com.makiewicz.cojest.model.UserData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.Optional;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    UserDao userDao;

    @Override
    public boolean userExist(String login) {
        LOG.debug("Checking if user : " + login + " exists.");
        return userDao.getAllUsers().stream().
                filter(user -> user.getLogin().equalsIgnoreCase(login)).
                count()>0;
    }

    @Override
    public boolean registerUser(User userToRegister, UserData userData) {
        LOG.debug("Register user : " + userToRegister + " .");
        userDao.addUser(userToRegister, userData);
        return true;
    }

    @Override
    public Optional<User> getUserById(Long userId) {
        LOG.debug("Get user with id : " + userId);
        return userDao.getUserById(userId);
    }

    @Override
    public boolean updateUser(User user) {
        LOG.debug("Update user with id : " + user.getId());
        return userDao.updateUser(user);
    }

    @Override
    public boolean deleteUser(Long userId) {
        LOG.debug("Delete user with id : " + userId);
        return userDao.deleteUserById(userId);
    }

    @Override
    public List<User> getAllUsers() {
        LOG.debug("Get all users.");
        return userDao.getAllUsers();
    }

    @Override
    public boolean userLogin(String login, String passworsHash) {
        // TODO: zrobienie logowania użytkowników
        throw new NotImplementedException();
    }
}
