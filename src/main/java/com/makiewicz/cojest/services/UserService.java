package com.makiewicz.cojest.services;

import com.makiewicz.cojest.model.Event;
import com.makiewicz.cojest.model.User;
import com.makiewicz.cojest.model.UserData;

import java.util.List;
import java.util.Optional;

public interface UserService {

    //User story: chcę sprawdzić czy dany użytkownik (o danym loginie) już istnieje, więc potrzebuja metody
    //do sprawdzania tego
    boolean userExist(String login);

    //User story: chcę zarejestrowac uzytkownika
    boolean registerUser(User userToRegister, UserData userData);

    //User story: chcę pobrać użytkownika o określonym id
    Optional<User> getUserById(Long userId);

    //User story: chcę uaktualnić dane użytkownika
    boolean updateUser(User user);

    //User story: chcę usunąć użytkownika o wskazanym id
    boolean deleteUser(Long userId);

    //User story: chcę wylistować wszystkich użytkowników
    List<User> getAllUsers();

    //User story: logowanie użytkownika
    boolean userLogin(String login, String passworsHash);

//    //User story: chcę dodać wydarzenie
//    boolean addEvent(Long userId, );
}
