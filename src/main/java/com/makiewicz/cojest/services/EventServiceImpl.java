package com.makiewicz.cojest.services;

import com.makiewicz.cojest.dao.EventDao;
import com.makiewicz.cojest.model.Event;
import com.makiewicz.cojest.model.Location;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.Optional;

@Service(value = "eventService")
@Transactional
public class EventServiceImpl implements EventService{

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    EventDao eventDao;

    @Override
    public boolean addEvent(Event event) {
        LOG.debug("Add event : " + event + " .");
        eventDao.addEvent(event);
        return true;
    }

    @Override
    public Optional<Event> getEventById(Long eventId) {
        LOG.debug("Get event with id : " + eventId);
        return eventDao.getEventById(eventId);
    }

    @Override
    public boolean updateEvent(Event event) {
        LOG.debug("Update event with id : " + event.getId());
        return eventDao.updateEvent(event);
    }

    @Override
    public boolean deleteEvent(Long eventId) {
        LOG.debug("Delete event with id : " + eventId);
        return eventDao.deleteEventById(eventId);
    }

    @Override
    public List<Event> getAllEvents() {
        LOG.debug("Get all events.");
        return eventDao.getAllEvents();
    }

    @Override
    public List<Event> getEventsNearMyLocation(Location location) {
        // TODO: zrobienie wyszukiwania eventów w okolicy
        throw new NotImplementedException();
    }

    @Override
    public List<Event> getSimilarEvents(Event event) {
        // TODO: zrobienie wyszukiwania podobnych eventów
        throw new NotImplementedException();
    }
}
